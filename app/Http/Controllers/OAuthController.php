<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class OAuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function redirect()
    {
        // redirect 

        $query = http_build_query([
            'client_id' => '2',
            'redirect_uri' => 'http://tranjactor.test/oauth/callback',
            'response_type' => 'code',
            'scope' => '',
        ]);
        return redirect('http://auctions-oauth.test/oauth/authorize?' . $query);
    }

    public function callback(Request $request)
    {



        $response = Http::post('http://auctions-oauth.test/oauth/token', [
            'grant_type' => 'authorization_code',
            'client_id' => '2',
            'client_secret' => '2rJR94P6hJ111pAXFZIQ2haJiUvHzx0FW0EweF3o',
            'redirect_uri' => 'http://tranjactor.test/oauth/callback',
            'code' => $request->code,
        ]);

        $response = $response->json();

        // return $response; 


        $request->user()->token()->delete();
        $request->user()->token()->create([
            'access_token' => $response['access_token']
        ]);

        return redirect('/dashboard');
    }
}
