<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class TestController extends Controller
{
    
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function accessToken() {

        // $response = Http::asForm()->post('http://auctions-oauth.test/oauth/token', [
        //     'grant_type' => 'password',
        //     'client_id' => '2',
        //     'client_secret' => 'ygqFlsjTuC3JSOM4Te5IWi50qRTTsuJo4nlZG01x',
        //     'username' => 'admin@admin.com',
        //     'password' => 'admin123',
        //     'scope' => '',
        // ]);
        
        // return $response->json();

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.auth()->user()->token->access_token,
        ])->get('http://auctions-oauth.test/api/getPropertyWithSubscriberIds');

        return $response->json(); 

    }

    public function index () {
        $user = [];

        if (auth()->user()->token) {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . auth()->user()->token->access_token
 
            ])->get('http://auctionouth.test/api/getusers');

            if ($response->status() === 200) {
                $response = $response->json(); 
                dd($response);
            } else {
                echo 'not verified';
            }
        } 
    }
}
